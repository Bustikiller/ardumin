
void on_conveyorBelt() {

  analogWrite(PIN_CONVEYOR_BELT, MAX_SPEED);
  current_conveyor_motor_speed = MAX_SPEED;
  Serial.println("Conveyor belt switched on");

}

void off_conveyorBelt() {

  analogWrite(PIN_CONVEYOR_BELT, 0);
  Serial.println("Conveyor belt switched off");

}

void on_pieceInsertor() {

  analogWrite(PIN_PIECE_INSERTOR, MAX_SPEED);
  current_piece_insertor_motor_speed = MAX_SPEED;
  Serial.println("Piece insertor switched on");

}

void off_pieceInsertor() {

  analogWrite(PIN_PIECE_INSERTOR, 0);
  Serial.println("Piece insertor switched off");

}

