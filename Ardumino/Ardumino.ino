
const int MAX_SPEED = 255;
const int DISTANCE_BETWEEN_WHEELS = 20;

// Input pins
const int PIN_PHOTORESISTOR_LEFT = 0;
const int PIN_PHOTORESISTOR_RIGHT = 1;

// Output pins
const int PIN_RUNNING = 2;
const int PIN_STOPPED = 4;
const int PIN_SWITCH_ON_BUTTON = 7;
const int PIN_CONVEYOR_BELT = 9;
const int PIN_LEFT_ENGINE = 3;
const int PIN_PIECE_INSERTOR = 6;
const int PIN_RIGHT_ENGINE = 5;

const int TIME_SEGMENT = 100;

int current_time_left;
int current_left_motor_speed;
int current_right_motor_speed;
int current_conveyor_motor_speed;
int current_piece_insertor_motor_speed;

void setup() {

  Serial.begin(9600);

  pinMode(PIN_LEFT_ENGINE, OUTPUT);
  pinMode(PIN_RIGHT_ENGINE, OUTPUT);
  pinMode(PIN_CONVEYOR_BELT,OUTPUT);
  pinMode(PIN_PIECE_INSERTOR,OUTPUT);

}

void loop() {

}







