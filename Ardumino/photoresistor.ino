const int LIGHT_THRESOLD = 500;

boolean arePiecesLeft() {

  int currentLightInLeft = analogRead(PIN_PHOTORESISTOR_LEFT);
  int currentLightInRight = analogRead(PIN_PHOTORESISTOR_RIGHT);
  int maxLight = currentLightInLeft < currentLightInRight ? currentLightInRight : currentLightInLeft;
  Serial.print("Current light: ");
  Serial.println(maxLight);
  boolean returnValue = maxLight < LIGHT_THRESOLD;
  return returnValue;

}

