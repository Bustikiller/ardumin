
void drive_forward(int distance) {

  Serial.print("Driving forward\n");
  enableRunningLed();

  current_left_motor_speed = MAX_SPEED;
  current_right_motor_speed = MAX_SPEED;


  analogWrite(PIN_LEFT_ENGINE, current_left_motor_speed);
  analogWrite(PIN_RIGHT_ENGINE, current_right_motor_speed);
  on_conveyorBelt();
  on_pieceInsertor();

  active_wait(1000 * distance);

  stop_ardumino();
}

void stop_ardumino() {

  disableRunningLed();

  analogWrite(PIN_LEFT_ENGINE, 0);
  analogWrite(PIN_RIGHT_ENGINE, 0);
  off_conveyorBelt();
  off_pieceInsertor();

  Serial.print("Ardumino stopped\n");

}

void continue_ardumino() {
  enableRunningLed();

  analogWrite(PIN_LEFT_ENGINE, current_left_motor_speed);
  analogWrite(PIN_RIGHT_ENGINE, current_right_motor_speed);
  analogWrite(PIN_CONVEYOR_BELT, current_conveyor_motor_speed);
  analogWrite(PIN_PIECE_INSERTOR, current_piece_insertor_motor_speed);

  Serial.print("Ardumino continued.");
  Serial.print(" Left: ");
  Serial.print(current_left_motor_speed);
  Serial.print(" Right: ");
  Serial.print(current_right_motor_speed);
  Serial.print(" Conveyor: ");
  Serial.print(current_conveyor_motor_speed);
  Serial.print(" Insertor: ");
  Serial.println(current_piece_insertor_motor_speed);
}

void drive_circle_left(int radius, int arc) {
  drive_circle(radius, arc, true);
}

void drive_circle_right(int radius, int arc) {
  drive_circle(radius, arc, false);
}

void drive_circle (int radius, int arc, boolean turnLeft) {

  enableRunningLed();

  int shortRadius =  radius - DISTANCE_BETWEEN_WHEELS / 2;
  int longRadius = radius + DISTANCE_BETWEEN_WHEELS / 2;

  float shortDistance = 2 * PI * shortRadius * arc / 360;
  float longDistance = 2 * PI * longRadius * arc / 360;

  int fastSpeed = MAX_SPEED;
  int slowSpeed = MAX_SPEED * shortDistance / longDistance;

  if (turnLeft) {
    analogWrite(PIN_LEFT_ENGINE, slowSpeed);
    analogWrite(PIN_RIGHT_ENGINE, fastSpeed);
  }
  else {
    analogWrite(PIN_RIGHT_ENGINE, slowSpeed);
    analogWrite(PIN_LEFT_ENGINE, fastSpeed);
  }

  on_conveyorBelt();
  on_pieceInsertor();

  active_wait(1000 * longDistance);

  stop_ardumino();

}


void active_wait(int time) {

  int numberOfTimeSegments = time / TIME_SEGMENT;
  for (int i = 0; i < numberOfTimeSegments; i++) {
    current_time_left = time - i * TIME_SEGMENT;
    if (arePiecesLeft()) {

      delay(TIME_SEGMENT);
    }
    else {
      stop_ardumino();
      waitUntilStartButtonIsPressed();
      continue_ardumino();
    }
  }
}

void waitUntilStartButtonIsPressed() {
  Serial.println("Waiting until start button is pressed");
  while (digitalRead(PIN_SWITCH_ON_BUTTON) == LOW) {
  }
  delay(500);
}

void enableRunningLed() {
  digitalWrite(PIN_RUNNING, HIGH);
  digitalWrite(PIN_STOPPED, LOW);
}

void disableRunningLed() {
  digitalWrite(PIN_RUNNING, LOW);
  digitalWrite(PIN_STOPPED, HIGH);
}













